# Changelogs

This repo holds a list of changes between ISO releases.

- [200831](https://gitlab.com/garuda-linux/changelogs/-/blob/master/Past%20changelogs/200831.md)
- [201007](https://gitlab.com/garuda-linux/changelogs/-/blob/master/Past%20changelogs/201007.md)
- [201022](https://gitlab.com/garuda-linux/changelogs/-/blob/master/Past%20changelogs/201022.md)
- [201118](https://gitlab.com/garuda-linux/changelogs/-/blob/master/Past%20changelogs/201118.md)
- [201205](https://gitlab.com/garuda-linux/changelogs/-/blob/master/Past%20changelogs/201205.md)
- [210101](https://gitlab.com/garuda-linux/changelogs/-/blob/master/Past%20changelogs/210101.md)
- [210225](https://gitlab.com/garuda-linux/changelogs/-/blob/master/Past%20changelogs/210225.md)
- [210507](https://gitlab.com/garuda-linux/changelogs/-/blob/master/Past%20changelogs/21507.md)
- [210809](https://gitlab.com/garuda-linux/changelogs/-/blob/master/Past%20changelogs/210809.md)
- [210926](https://gitlab.com/garuda-linux/changelogs/-/blob/master/Past%20changelogs/210926.md)
- [220101](https://gitlab.com/garuda-linux/changelogs/-/blob/master/Past%20changelogs/220101.md)
- [220429](https://gitlab.com/garuda-linux/changelogs/-/blob/master/Past%20changelogs/220429.md)
- [230305](https://gitlab.com/garuda-linux/changelogs/-/blob/master/Past%20changelogs/230305.md)
- [231029](https://gitlab.com/garuda-linux/changelogs/-/blob/master/Past%20changelogs/231029.md)
- [240428](https://gitlab.com/garuda-linux/changelogs/-/blob/master/Past%20changelogs/240428.md)
