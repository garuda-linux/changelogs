# Changes shipped in 201007 release

- Moved our website and other services such as forum, wiki, startpage, CryptPad, PrivateBin, to our new domain - garudalinux.org and new servers powered by fosshost.org
- Updated all apps and shortcuts to point to the new domain
- You can now use Matrix to contact us if thats what you prefer. Simply create an account at element.garudalinux.org to join our instance. To make things even more easy and accessible you will find a room which is bridged directly to our Telegram group for quick help.
- We decided to offer every user 250MB of space in our Nextcloud instance (cloud.garudalinux.org). This can be used to sync dotfiles, contacts, calendar or notes. This is experimental for now, we want to see how many of our users choose to use this service.
- Thanks to the guys at fosshost.org we are also able to provide a BigBlueButton instance, found at meet.garudalinux.org. You can also use our Nextcloud instance to create and join rooms, they are linked.

- Added Cinnamon edition
- Added Mate edition
- Added BSPWM edition
- Added KDE/GNOME barebones editions (no theming, metapkgs & bare minimum of DE - also we provide no support for these editions. For advanced users only)

- All Editions:
  - Now calamares creates @ @home @cache @log subvolumes by default for btrfs filesystem installs.it reduces the snapshot size so the space consumption is lowered
  - Remove systemd-guest-user to prevent issues when logging in etc, now available via Garuda Welcome app
  - Replaced lightdm-gtk-greeter with lightdm-slick-greeter
  - Remove conky from lite edition and Add conky-manager to ultimate
  - Add the chaotic-keyring, chaotic-mirrorlist package to get new mirrors automatically
  - Enabled DefaultLimitNOFILE=524288 needed for Wine ESYNC feature
  - Decreased Default timeout to 30 sec from 90 sec to speedup shutdown
  - Slimmed down Lite & Ultimate editions
  - Fixed installation problem with encryption
  - Added cpu-autofreq & thermald which should result in less fan noise and much better battery life
  - Replaced Keepassxc browser extension with Bitwarden & Joplin in Ultimate editions (to use with our Nextcloud & Bitwarden)
  - Added garuda-common-settings and moved shared settings (eg. enabling services) to this package
  - Added a simple setup assistant which automatically launches during first boot which also includes upgrading Lite to Ultimate
  - Disabled annoying beep from the speakers
  - Disable Intel VPRO remote access technology driver as a security precaution.
- GNOME:
  - Provide WhiteSur Kvantum via package & ship non-transparent config
  - Add Alacritty font size since standart was too big on resolutions <Full HD
  - Updated patched WhiteSur-gtk theme & included the white one as well
  - Replaced flameshot with ksnip
  - Switched gdm-prime to gdm to prevent issues during boot
  - Removed all extensions due to GNOME 3.38 being launched. Its pretty barebones now but extensions will be added back once they get updated.
- KDE:
  - Replaced flameshot with spectacle
- Garuda-hooks:
  - Add a hook to remove systemd from nsswitch.conf to fix boot delays and fix network problems
  - Added automatic fixing of Pamac icons not showing
- Garuda-browser-settings:
  - Browsers now use our own startpage
- Garuda-Welcome:
  - Added settings to enable printing, scanning and samba support and rightclick emulation for Lite edition
  - Added links to our webservices
- Garuda-settings-manager
  - Hidden the nobody user
  - Change kernel name to package instead of version
- Garuda-boot-options
  - Added option to set sysrq
  - Added option to set mitigations=off

Live user/password (if needed) is always garuda/garuda
