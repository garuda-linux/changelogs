# Changes that were shipped in release 201118:

## All editions:

- Unified setup-assistant stuff into /usr/bin/setup-assistant
- Added a GUI to setup-assistant which also enables upgrading to ultimate with a lot of choices!
- Apps available via setup assistant are removed from the isos
- Only KDE will have an Ultimate edition build, otherwise use the setup assistant to get the apps you need.
- Firewall not enabled by default anymore (people were confused about services not working)
- Fixed auto-cpufreq service
- Added micro config
- Terminus set as TTY font (looks way better)
- Neofetch now has a nice nice ASCII art with the Garuda logo
- KDE:
  - Updated Latte layout
  - Removed redundant theme choices which were missleading
  - Use wallpaper instead of Slideshow
- Dr460nized:
  - Basically a very slim, sweetified Plasma config with a lot of blur & fish as shell
  - MacOS style appmenu at the top while having Latte as dock
- GNOME:
  - Updated ArcMenu to v4
  - Theming now shipped in its own package
  - Updated Flagfox
  - Broken? Not broken? Nobobody knows for sure.
- XFCE:
  - Complete overhaul of packages & configs by @Yorper (clean!)
- GNOME (barebones):
  - Added networkmanager
- Garuda-wallpapers:
  - Added tons of nice wallpapers by @SGS
  - Added 2 nice eagle themed wallpapers by @pupperemeritus
- Garuda-firefox:
  - Removed HTTPS everywhere as its native in FF83+, enabled by default
  - Only pulls 3 essential addons (uBlock origin, Dark reader, LocalCDN)
- Performance-tweaks:
  - Moved to GitLab
- Garuda-zsh-config
  - Removed oh-my-zsh
  - Added completions
  - Fix keybindings
- Garuda-metapkg
  - Updated to versions 20201116
- Garuda Welcome:
  - Improved GDM Wayland toggle
  - Updated HiDPI mode
  - Removed settings & Qwikaccess -> Garuda Assistant
- Garuda Assisstant:
  - Moved settings from Garuda Welcome & additional ones to this application
  - Added hblock as local adblocking option (via /etc/hosts)
- Garuda Gamer:
  - GUI to install & enable gaming components

## Some version upgrades of included software: - Firefox 83 - Plasma 5.20.3 & KDE frameworks 5.76 - Linux-zen 5.9.8
