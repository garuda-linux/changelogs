Changes shipped in release 210101:

Edition changes:

- Every edition:
  - Added nextcloud & Bitwarden by default
  - No more asking for password in live sessions for polkit enabled programs
  - Switched to mpv + frontend (working hw accel)
  - Setup assistant got code reworked, now feels much better to use and closes correctly & has internet connectivity check
  - You are now prompted to use the setup assistant on first boot & prompted to uninstall when done with it
  - Seperated setup-assistant and initial user setup
  - Flatpak/Snapd prompt removed, no more "official" support for that - we have ChaoticAUR (still need? -> read the Wiki)
- Garuda BSPWM:
  - Font changed to FiraSans/FantasqueSansMono
- Garuda dr460nized:
  - Added pdf thumbnails
  - Fixed bluetooth
  - Some adjustments to layout
  - Even more themed custom icons!
  - Finally enabled SDDM theming
  - Rearranged latte dock + Pamac launcher fixed
  - Alacritty config fixed + font size decreased to fit console
  - Nightcolor now "opt-in" as it confused many users
- Garuda dr460nized-gaming:
  - Basically dr460nized + required packages for gaming on top
- Garuda GNOME:
  - Updated ArcMenu & Unite to latest versions
  - Font changed to FiraSans/FantasqueSansMono
- Garuda KDE-multimedia:
  - The continued "regular" KDE theming + multimedia packages-
- Garuda LXQt-Kwin:
  - Fixed wallpaper paths
- Garuda Wayfire:
  - Fix cursor for nouveau users
- Garuda XFCE:
  - Updated Whiskermenu
- Garuda Qtile:
  - Qtile is a dynamic extensible window manager with small resource consumption and easy to customize. Garuda Qtile uses jgmenu which makes it very easy and convenient to launch application without remembering keybindings.

Applications & settings:

- Garuda Gamer:
  - Code rework (now faster & smaller file size)
  - Added keymapper & another emulator
  - You can now check the applications and install them all together using pamac-installer
- Garuda Network Assistant:
  - Added wifi powersafe
  - Added disable MAC address randomization
- Garuda Assistant:
  - Added some more settings
- Garuda Boot Options:
  - Dont show save last choice on BTRFS (broken)
- Garuda Setup Assistant:
  - Moved to own package
  - Removed Snaps/Flatpaks. We no longer support those as we got almost every package in the repo.
- Garuda Welcome:
  - Using system icons
  - Make all linked Garuda apps optional depends
  - Prompt to install if package massing
  - Fix not detecting packages in LiveCD
- Garuda-chroot:
  - Fixed BTRFS handling
- Garuda Settings Manager:
  - Using system icons
- Mhwd-garuda:
  - Sync with upstream Manjaro
- Garuda-icons:
  - Added BeautyLine Garuda logo
  - Fixed update-gtk-icon-cache warning
- Mhwd-db-garuda:
  - Added chaotic nvidia driver db
  - Fixed video-virtualmachine on QXL graphics
  - Added nvidia-390xx-dkms support
- Garuda-fish-config:
  - Correctly apply .profile
  - Use paleofetch instead of neofetch
  - Changed color of Lambda to red
- Garuda-zsh-config:
  - Use paleofetch
- Garuda Homepage:
  - Added links to projects in the feature list
  - Enhanced download button

Version upgrades: - Linux-tkg-bmq 5.10.3 - Firefox 84
